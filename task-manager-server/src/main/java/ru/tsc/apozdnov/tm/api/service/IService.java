package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.api.repository.IRepository;
import ru.tsc.apozdnov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}