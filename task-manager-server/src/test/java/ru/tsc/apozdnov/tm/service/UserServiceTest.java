package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.exception.entity.UserNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.*;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.repository.ProjectRepository;
import ru.tsc.apozdnov.tm.repository.TaskRepository;
import ru.tsc.apozdnov.tm.repository.UserRepository;

public class UserServiceTest {

    private final static long INITIAL_SIZE = 2;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Before
    public void init() {
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.create("", "12345"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user", "12345"));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.create("test", ""));
        @NotNull final User user = (userService.create("test", "12345"));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.create("", "12345", "test@test"));
        Assert.assertThrows(ExistsLoginException.class,
                () -> userService.create("user", "12345", "test@test"));
        Assert.assertThrows(EmptyPasswordException.class,
                () -> userService.create("test", "", "test@test"));
        Assert.assertThrows(ExistsEmailException.class,
                () -> userService.create("test", "12345", "user@user.ru"));
        @NotNull final User user = (userService.create("test", "12345", "test@test"));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNotNull(user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.create("", "12345", Role.ADMIN));
        Assert.assertThrows(ExistsLoginException.class,
                () -> userService.create("user", "12345", Role.ADMIN));
        Assert.assertThrows(EmptyPasswordException.class,
                () -> userService.create("test", "", Role.ADMIN));
        @NotNull final User user = (userService.create("test", "12345", Role.ADMIN));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.findByLogin(""));
        Assert.assertNotNull(userService.findByLogin("user"));
        Assert.assertNull(userService.findByLogin("not_user"));
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmptyEmailException.class, () -> userService.findByEmail(""));
        Assert.assertNotNull(userService.findByEmail("user@user.ru"));
        Assert.assertNull(userService.findByEmail("not_user@user.ru"));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist("user"));
        Assert.assertFalse(userService.isLoginExist("not_user"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist("user@user.ru"));
        Assert.assertFalse(userService.isEmailExist("not_user@user.ru"));
        Assert.assertFalse(userService.isEmailExist(""));
        Assert.assertFalse(userService.isEmailExist(null));
    }

    @Test
    public void remove() {
        @NotNull final User user = userService.findByLogin("user");
        userService.remove(user);
        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
        Assert.assertNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        @NotNull final User user = userService.removeByLogin("user");
        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
        Assert.assertNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmptyEmailException.class, () -> userService.removeByEmail(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByEmail("not_user@test"));
        @NotNull final User user = userService.removeByEmail("user@user.ru");
        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
        Assert.assertNull(userService.findByEmail(user.getEmail()));
    }

    @Test
    public void setPassword() {
        @NotNull final User user = userService.create("test", "test");
        @NotNull final String oldPasswordHash = user.getPasswordHash();
        Assert.assertThrows(EmptyIdException.class, () -> userService.setPassword("", "123"));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.setPassword(user.getId(), ""));
        userService.setPassword(user.getId(), "12345");
        Assert.assertNotEquals(oldPasswordHash, user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull final User user = userService.create("test", "test");
        @NotNull final String userId = user.getId();
        Assert.assertThrows(EmptyIdException.class,
                () -> userService.updateUser("", "", "", ""));
        @NotNull final String firstName = "first";
        @NotNull final String lastName = "last";
        @NotNull final String middleName = "middle";
        userService.updateUser(userId, firstName, lastName, middleName);
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        @NotNull final User user = userService.create("test", "test");
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        @NotNull final User user = userService.create("test", "test");
        user.setLocked(true);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(user.getLocked());
    }

}