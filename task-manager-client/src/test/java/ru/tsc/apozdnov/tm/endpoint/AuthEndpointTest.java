package ru.tsc.apozdnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.dto.request.UserLoginRequest;
import ru.tsc.apozdnov.tm.dto.request.UserProfileRequest;
import ru.tsc.apozdnov.tm.dto.response.UserLoginResponse;
import ru.tsc.apozdnov.tm.dto.response.UserProfileResponse;
import ru.tsc.apozdnov.tm.marker.ISoapCategory;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.service.PropertyService;

@Category(ISoapCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Test
    public void login() {
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("not_user_login", "1234")));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("user", "not_password")));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("user", null)));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(null, null)));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("user", "user"));
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("user", "user"));
        @Nullable String token = response.getToken();
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.profile(new UserProfileRequest("not_token")));
        UserProfileResponse responseProfile = authEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(responseProfile);
        @Nullable User user = responseProfile.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("user", user.getLogin());
    }

}